#ifndef _JOBS_H
#define _JOBS_H

typedef struct {
    char *sender; /* получатель */
    char *content; /* содержимое письма (тело) */
    char *file_name; /* файл письма (потребуется для удаления в случае успеха) */
    int num_of_jobs; /* число работ = числу получа*/
} Mail;

typedef struct {
    Mail *mail; /* общее письмо для разных получателей */
    char *to_hostname; /* имя MSA для которого будет отправлено письмо */
    int recipients_count; /* число получателей */
    char **recipients; /* список получателей */
    int skipped; /* номер по порядку пропущенного получателя */
} Mailjob;

typedef enum {
    DELIVERED, /* письмо успешно доставлено */
    REJECTED /* письмо отвергнуто сервером */
} MJ_status;

#define MAX_BUFFER 4096

int mail_to_jobs(const char *, Mailjob **, int *);
void freejob(Mailjob *, MJ_status, const char *);
void logjobskip(Mailjob *, int);
void printjobs(Mailjob *, int *);

#endif /* _JOBS_H */
