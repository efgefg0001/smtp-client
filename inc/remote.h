#ifndef _REMOTE_H_
#define _REMOTE_H_

#include <netinet/in.h>

#include "jobs.h"

#ifndef MAILJOB_QUEUE
#define MAILJOB_QUEUE
#define _ITEM_TYPE Mailjob
#define _TYPED_NAME(x) mailjob_##x
#include "queue.h"
#undef _ITEM_TYPE
#undef _TYPED_NAME
#endif

/* текущее состояние удаленного хоста */
typedef enum {
    RS_NONE,
    RS_COMMAND_WAIT,
    RS_COMMAND_OK,
    RS_RESPONSE_WAIT,
    RS_RESPONSE_OK,
} Remote_state;

/* команда smtp */
typedef enum {
    SMTP_NONE,
    SMTP_GRET,
    SMTP_HELO,
    SMTP_MAIL,
    SMTP_RCPT,
    SMTP_DATA,
    SMTP_TEXT,
    SMTP_RSET,
    SMTP_QUIT
} Smtp_command;

/* удаленный хост */
typedef struct {

    char* hostname; /* имя хоста */

    Remote_state state; /* текущее состояние: NONE, RESPONSE_WAIT, RESPONSE_OK, COMMANT_WAIT, COMMANT_OK */
    Smtp_command state_command; /* текущая команда smtp */

    struct sockaddr_in serv_addr; /* адресс удаленного хоста */
    int pollfd_number; /* дискриптор текущего удаленного хоста */

    mailjob_queue mq; /* очередь писем */

    char* buffer; /* буффер */
    int buffer_length; /* размер буфера */
    uint buffer_offset; /* смещение внутри буфера */
    uint message_length; /* размер сообщения */
    uint rcptindex; /* номер текущего получателя */

} Remote;

#define BASE_REMOTE_BUFFER_SIZE 1024
#define COMPLETION_REPLY '2'
#define INTERMEDIATE_REPLY '3'

int remote_create(Remote *, const char *, int);
int remote_act(Remote *, struct pollfd *);
void remote_terminate_jobs(Remote * r);
void remote_destroy(Remote* r);
int close_smtp(Remote *r, struct pollfd *pfd);
#endif /* _REMOTE_H_ */
