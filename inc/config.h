#ifndef _CONFIG_H
#define _CONFIG_H

/* поиск адресса сервера через hosts (добавить информацию в hosts нужно самостоятельно)
 * если 0 - то все письма, независимо от указанного адресса будут отправлены на IP_ADRESS */
#define FIND_IP_THROUGH_HOSTS 1
#define IP_ADRESS "127.0.0.1"
#define PORT 1025
/* папка в которой MTA будет искать письма
 * елси указывать папку отличную от maildir в корне проекта, то нужно еще поменять путь в Makefile */
#define MAILDIR "/home/alex/maildir"
/* имя нашего хоста (требуется для приветствия в smtp) */
#define OUR_HOSTNAME "dell-laptop"

#define LOG_PATH "build/smtp-client.log" /* путь к лог-файлу */

#define INITIAL_STRUCTURE_SIZE 128 /* начальный размер структур для хранения писем и удаленных хостов */

/* выводить в stderr ту ли иную информацию */
#define USEFUL_INFO 1 /* полезная информация о работе сервера */
#define CLEAN_INFO 1 /* информация об очистке структут и данных */
#define DEBUG_INFO 0 /* информация для отладки */
#define SMTP_INFO 1 /* вывод в stderr информации об smtp сообщениях */
#define WRITE_TO_LOG 1 /* пишем в журнал */
#endif //_CONFIG_H
