#ifndef _SERVER_H_
#define _SERVER_H_

#include <string.h>
#include <poll.h>
#include <time.h>

#include "jobs.h"

#ifdef _INCLUDE_IMPLEMENTATION
#undef _INCLUDE_IMPLEMENTATION
#include "remote.h"
#define _INCLUDE_IMPLEMENTATION
#else
#include "remote.h"
#endif

#ifndef S_REMOTE_DICT
#define S_REMOTE_DICT
typedef char* s;
#define _KEY_TYPE s
#define _VALUE_TYPE Remote
#define _TYPED_NAME(x) mta_remotes_##x
#include "dict.h"
#undef _TYPED_NAME
#undef _KEY_TYPE
#undef _VALUE_TYPE
#endif

#ifndef POLLFD_LIST
#define POLLFD_LIST
typedef struct pollfd pollfd_t;
#define _ITEM_TYPE pollfd_t
#define _TYPED_NAME(x) pollfd_##x
#include "stack.h"
#undef _ITEM_TYPE
#undef _TYPED_NAME
#endif

#define POLL_TIMEOUT_MSECS 50


typedef struct
{
    long long last_active_time; /* время последней активности */
    mta_remotes_dict remotes; /* удаленные MSA, которым отправляется почта */
    pollfd_stack descriptors; /* список дискрипторов для работы с poll */
} MTA;

void start_mta(MTA *);
void mta_terminate_jobs(MTA *);
int mta_destroy(MTA *);
int mta_init(MTA *);

#endif
