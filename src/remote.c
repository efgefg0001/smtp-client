#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <poll.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdarg.h>

#define _INCLUDE_IMPLEMENTATION

#include "remote.h"

#undef _INCLUDE_IMPLEMENTATION

#include "config.h"
#include "log.h"

extern int mta_is_running;

int remote_act(Remote *r, struct pollfd *pfd);
int remote_act_internal(Remote *r, struct pollfd *pfd);
int go_to_next_state(Remote_state next_state, Remote *r, struct pollfd *pfd);
int send_command(Smtp_command command_to_be_sent, Remote *r, struct pollfd *pfd);
int send_command_string(Smtp_command command_to_be_sent, Remote *r, struct pollfd *pfd, const char *format, ...);
int close_smtp(Remote *r, struct pollfd *pfd);
int do_none(Remote *r, struct pollfd *pfd);
int do_response_wait(Remote *r, struct pollfd *pfd);
int do_command_wait(Remote *r, struct pollfd *pfd);
int do_command_ok(Remote *r, struct pollfd *pfd);
int do_response_ok(Remote *r, struct pollfd *pfd);
int do_smtp(Remote *r, struct pollfd *pfd);
int remote_create(Remote *r, const char *hostname, int pollfd_number);
void remote_destroy(Remote *r);
void remote_terminate_jobs(Remote *r);
const char *strstate(Remote_state state);
const char *strsmtp(Smtp_command state);

int remote_act(Remote *r, struct pollfd *pfd) {
    int ret;
    while (!(ret = remote_act_internal(r, pfd)) && mta_is_running);
    return ret;
}

int remote_act_internal(Remote *r, struct pollfd *pfd) {
    if (pfd->revents & POLLERR) {
        return close_smtp(r, pfd);
    }
    switch (r->state) {
        case RS_NONE: {
            return do_none(r, pfd);
        }
        case RS_RESPONSE_WAIT: {
            return do_response_wait(r, pfd);
        }
        case RS_COMMAND_WAIT: {
            return do_command_wait(r, pfd);
        }
        case RS_COMMAND_OK: {
            return do_command_ok(r, pfd);
        }
        case RS_RESPONSE_OK: {
            do_response_ok(r, pfd);
            return do_smtp(r, pfd);
        }
    }
    return 0;
}

int go_to_next_state(Remote_state next_state, Remote *r, struct pollfd *pfd) {
    int waitfor;
    if (next_state == RS_RESPONSE_WAIT) {
        waitfor = POLLIN;
    } else if (next_state == RS_COMMAND_WAIT) {
        waitfor = POLLOUT;
    } else waitfor = 0;
    /* если ждем POLLIN или POLLOUT и следующее состояние не RS_NONE, то выйдем из цикла, чтобы првоерить poll */
    int stoploop = !(!waitfor && next_state);
    /* вывод информации о текущем состоянии "автомата переходов" */
    if (DEBUG_INFO) {
        if (r->state != RS_NONE || next_state != RS_NONE) {
            fprintf(stderr, "Хост %s: %s -> %s", r->hostname, strstate(r->state), strstate(next_state));\
        if (r->state_command) {
                fprintf(stderr, "(%s)", strsmtp(r->state_command));
            }
            if (stoploop) {
                fprintf(stderr, ", жду %d", waitfor);
            }
            fprintf(stderr, ", stoploop %d\n", stoploop);
        }
    }
    r->state = next_state;
    /* чего ждем */
    pfd->events = waitfor;
    /* очищаем ранее поступившие события */
    pfd->revents = 0;
    return stoploop;
}


int send_command_string(Smtp_command command_to_be_sent, Remote *r, struct pollfd *pfd, const char *format, ...) {
    while (mta_is_running) {
        va_list vl;
        va_start(vl, format);
        r->message_length = vsnprintf(r->buffer, r->buffer_length, format, vl);
        va_end(vl);

        if (r->message_length < r->buffer_length) {
            break;
        } else {
            char *new_buff = realloc(r->buffer, r->buffer_length + BASE_REMOTE_BUFFER_SIZE);\
            if (new_buff == NULL) {
                fprintf(stderr, "Хост %s: невозможно выделить память для нового буфера, завершаю сеанс\n", r->hostname);
                return close_smtp(r, pfd);
            }
            r->buffer_length += BASE_REMOTE_BUFFER_SIZE;
            r->buffer = new_buff;
        }
    }
    r->buffer_offset = 0;
    r->state_command = command_to_be_sent;
    return go_to_next_state(RS_COMMAND_WAIT, r, pfd);
}


int send_command(Smtp_command command_to_be_sent, Remote *r, struct pollfd *pfd) {
    switch (command_to_be_sent) {
        case SMTP_HELO:
            return send_command_string(command_to_be_sent, r, pfd, "HELO %s\r\n", OUR_HOSTNAME);
        case SMTP_MAIL:
            return send_command_string(command_to_be_sent, r, pfd, "MAIL FROM:<%s>\r\n",
                                       r->mq.items[r->mq.head].mail->sender);
        case SMTP_RCPT:
            return send_command_string(command_to_be_sent, r, pfd, "RCPT TO:<%s@%s>\r\n",
                                       r->mq.items[r->mq.head].recipients[r->rcptindex++],
                                       r->mq.items[r->mq.head].to_hostname);
        case SMTP_DATA:
            return send_command_string(command_to_be_sent, r, pfd, "DATA\r\n");
        case SMTP_TEXT:
            return send_command_string(command_to_be_sent, r, pfd, "%s\r\n.\r\n",
                                       r->mq.items[r->mq.head].mail->content);
        case SMTP_RSET:
            return send_command_string(command_to_be_sent, r, pfd, "RSET\r\n");
        case SMTP_QUIT:
            return send_command_string(command_to_be_sent, r, pfd, "QUIT\r\n");
        default: {
            fprintf(stderr, "Хост %s: неизвестная команда %s в SEND_COMMAND\n", r->hostname,
                    strsmtp(command_to_be_sent));
            return close_smtp(r, pfd);\

        }
    }
}


int close_smtp(Remote *r, struct pollfd *pfd) {
    r->buffer[0] = COMPLETION_REPLY;
    r->state_command = SMTP_QUIT;
    return go_to_next_state(RS_RESPONSE_OK, r, pfd);
}

int do_none(Remote *r, struct pollfd *pfd) {
    if (pfd->fd == -1) {
        pfd->fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    }
    if (pfd->fd < 0) {
        fprintf(stderr, "Ошибка при создании сокета для хоста %s: %s\n", r->hostname, strerror(errno));
        pfd->fd = -1;
        return go_to_next_state(RS_NONE, r, pfd);
    }

    memset(&r->serv_addr, 0, sizeof(r->serv_addr));
    r->serv_addr.sin_family = AF_INET;
    r->serv_addr.sin_port = htons(PORT);
    /* поиск адресса через файл hosts */
    if (FIND_IP_THROUGH_HOSTS) {
        struct hostent *h = gethostbyname(r->hostname);
        if (h == NULL) {
            fprintf(stderr, "Не удалось найти %s в файле hosts\n", r->hostname);
            exit(EXIT_FAILURE);
        } else {
            r->serv_addr.sin_addr = *(struct in_addr *) h->h_addr_list[0];
        }
    } else {
        r->serv_addr.sin_addr.s_addr = inet_addr(IP_ADRESS);
    }

    int cr = connect(pfd->fd, (struct sockaddr *) &r->serv_addr, sizeof(r->serv_addr));
    if (cr == 0 || (cr == -1 && errno == EINPROGRESS)) {
        r->state_command = SMTP_GRET;
        r->buffer_offset = 0;
        return go_to_next_state(RS_RESPONSE_WAIT, r, pfd);
    }

    fprintf(stderr, "Ошибка подключения к удаленному серверу с кодом %d и ошибкой %s\n", cr, strerror(errno));
    return go_to_next_state(RS_NONE, r, pfd);
}

int do_response_wait(Remote *r, struct pollfd *pfd) {
    if (!(pfd->revents & POLLIN)) {
        return close_smtp(r, pfd);
    }
    while (mta_is_running) {
        int bytes_read = read(pfd->fd, r->buffer + r->buffer_offset, r->buffer_length - r->buffer_offset - 1);
        r->buffer[r->buffer_offset + (bytes_read > 0 ? bytes_read : 0)] = '\0';
        if (bytes_read == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                fprintf(stderr, "Хост %s: ответ '%s' не соведжит завершающих символов, ожидаю '\\r\\n'\n",
                        r->hostname,
                        r->buffer);
                return go_to_next_state(RS_RESPONSE_WAIT, r, pfd);
            } else {
                fprintf(stderr, "Хост %s: ошибка: %s\n", r->hostname, strerror(errno));
                return close_smtp(r, pfd);
            }
        }

        if (bytes_read > 0) {
            r->buffer_offset += bytes_read;
        }
        if (strstr(r->buffer, "\r\n")) {
            r->buffer[r->buffer_offset - 2] = '\0';
            if (SMTP_INFO) { fprintf(stderr, "Хост %s: <- '''%s'''\n", r->hostname, r->buffer); }
            if (WRITE_TO_LOG) { to_log("Хост %s: <- '''%s'''", r->hostname, r->buffer); }
            return go_to_next_state(RS_RESPONSE_OK, r, pfd);
        }

        if (r->buffer_offset == r->buffer_length - 1) {
            char *new_buff = realloc(r->buffer, r->buffer_length + BASE_REMOTE_BUFFER_SIZE);
            if (new_buff == NULL) {
                fprintf(stderr, "Хост %s: невозможно выделить память для нового буфера, завершаю сеанс...\n",
                        r->hostname);
                return send_command(SMTP_QUIT, r, pfd);
            }
            r->buffer_length += BASE_REMOTE_BUFFER_SIZE;
            r->buffer = new_buff;
        }
    }
    return 0;
}


int do_command_wait(Remote *r, struct pollfd *pfd) {
    if (!(pfd->revents & POLLOUT)) {
        return close_smtp(r, pfd);
    }
    while (mta_is_running) {
        int bytes_written = write(pfd->fd, r->buffer + r->buffer_offset, r->message_length - r->buffer_offset);
        if (bytes_written == -1) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                fprintf(stderr, "Хост %s: writing is partial, %d more to go",
                        r->hostname, r->message_length - r->buffer_offset);
                return go_to_next_state(RS_COMMAND_WAIT, r, pfd);
            } else {
                fprintf(stderr, "Хост %s: ошибка: %s\n", r->hostname, strerror(errno));
                return close_smtp(r, pfd);
            }
        }
        r->buffer_offset += bytes_written;
        if (r->buffer_offset == r->message_length) {
            r->buffer[r->buffer_offset - 2] = '\0';
            if (SMTP_INFO) { fprintf(stderr, "Хост %s: -> '''%s'''\n", r->hostname, r->buffer); }
            if (WRITE_TO_LOG) { to_log("Хост %s: -> '''%s'''", r->hostname, r->buffer); }
            return go_to_next_state(RS_COMMAND_OK, r, pfd);
        }
    }
    return 0;
}

int do_command_ok(Remote *r, struct pollfd *pfd) {
    r->buffer_offset = 0;
    return go_to_next_state(RS_RESPONSE_WAIT, r, pfd);
}

int do_response_ok(Remote *r, struct pollfd *pfd) {
    if (!(r->buffer[0] == COMPLETION_REPLY || (r->buffer[0] == INTERMEDIATE_REPLY && r->state_command == SMTP_DATA))) {
        if (r->state_command == SMTP_MAIL ||
            r->state_command == SMTP_RCPT ||
            r->state_command == SMTP_DATA ||
            r->state_command == SMTP_TEXT) {
            if (r->state_command == SMTP_RCPT) {
                Mailjob *mj = &r->mq.items[r->mq.head];
                fprintf(stderr, "Хост %s: RCPT error, пропускаю <%s@%s>...\n",
                        r->hostname, mj->recipients[r->rcptindex - 1], mj->to_hostname);
                logjobskip(mj, r->rcptindex - 1);

                mj->skipped++;
                if (r->rcptindex < mj->recipients_count) {
                    r->buffer[0] = COMPLETION_REPLY;
                    return go_to_next_state(RS_RESPONSE_OK, r, pfd);
                } else if (r->rcptindex == mj->recipients_count && mj->skipped < mj->recipients_count) {
                    return send_command(SMTP_DATA, r, pfd);
                }
            }
            fprintf(stderr, "Хост %s: MAIL error, письмо отвергнуто, произвожу перезапуск...\n", r->hostname);
            Mailjob mj;
            mailjob_queue_pop(&r->mq, &mj);
            freejob(&mj, REJECTED, r->buffer);
            return send_command(SMTP_RSET, r, pfd);
        } else {
            if (r->state_command != SMTP_QUIT) {
                fprintf(stderr, "Хост %s: SMTP error %s, завершаю сеанс...\n", r->hostname, r->buffer);
                return send_command(SMTP_QUIT, r, pfd);
            } else {
                fprintf(stderr, "Завершаю сессию...\n");
                return close_smtp(r, pfd);
            }
        }
    }
    return 0;
}

int do_smtp(Remote *r, struct pollfd *pfd) {
    switch (r->state_command) {
        case SMTP_GRET:
            return send_command(SMTP_HELO, r, pfd);
        case SMTP_HELO: {
            if (r->mq.count > 0) {
                return send_command(SMTP_MAIL, r, pfd);
            } else {
                return send_command(SMTP_QUIT, r, pfd);
            }
        }
        case SMTP_MAIL: {
            r->rcptindex = 0;
            return send_command(SMTP_RCPT, r, pfd);
        }
        case SMTP_RCPT: {
            if (r->rcptindex < r->mq.items[r->mq.head].recipients_count) {
                if (USEFUL_INFO) { fprintf(stderr, "Хост %s: пересылаю получателя %d/%d\n", r->hostname, r->rcptindex,
                        r->mq.items[r->mq.head].recipients_count); }
                return send_command(SMTP_RCPT, r, pfd);
            } else {
                return send_command(SMTP_DATA, r, pfd);
            }
        }
        case SMTP_DATA:
            return send_command(SMTP_TEXT, r, pfd);
        case SMTP_TEXT: {
            Mailjob mj;
            mailjob_queue_pop(&r->mq, &mj);
            if (USEFUL_INFO) { fprintf(stderr, "Хост %s: почтовая работа успешно завершена! \n", r->hostname); }
            freejob(&mj, DELIVERED, NULL);
            r->state_command = SMTP_HELO;
            return go_to_next_state(RS_RESPONSE_OK, r, pfd);
        }
        case SMTP_RSET: {
            r->state_command = SMTP_HELO;
            return go_to_next_state(RS_RESPONSE_OK, r, pfd);
        }
        case SMTP_QUIT: {
            r->state_command = SMTP_NONE;
            close(pfd->fd);
            pfd->fd = -1;
            return go_to_next_state(RS_NONE, r, pfd);
        }
        default: {
            fprintf(stderr, "Хост %s: неожиданный RESPONSE_OK в статусе %s\n",
                    r->hostname, strsmtp(r->state_command));
            return close_smtp(r, pfd);
        }
    }
    return 0;
}

int remote_create(Remote *r, const char *hostname, int pollfd_number) {
    mailjob_queue_init(&r->mq, INITIAL_STRUCTURE_SIZE);
    if (USEFUL_INFO) { fprintf(stderr, "Создание структуры удаленного хоста %s\n", hostname); }
    /* выделение памяти для имени хоста */
    if (!(r->hostname = calloc(strlen(hostname) + 1, sizeof(char)))) {
        fprintf(stderr, "Не удалось выделить память для хоста %s\n", hostname);
        mailjob_queue_free(&r->mq);
        return -1;
    }
    if (!(r->buffer = malloc(sizeof(char) * BASE_REMOTE_BUFFER_SIZE))) {
        fprintf(stderr, "Не удалось выделить память для хоста %s\n", hostname);
        mailjob_queue_free(&r->mq);
        free(r->hostname);
        return -1;
    }
    strcpy(r->hostname, hostname);
    r->pollfd_number = pollfd_number;
    r->state = RS_NONE;
    r->state_command = SMTP_NONE;
    r->buffer_offset = 0;
    r->message_length = 0;
    r->pollfd_number = pollfd_number;
    r->buffer_length = BASE_REMOTE_BUFFER_SIZE;
    return 0;
}

void remote_destroy(Remote *r) {
    if (CLEAN_INFO) { fprintf(stderr, "Очистка структур для %s...\n", r->hostname); }
    remote_terminate_jobs(r);
    mailjob_queue_free(&r->mq);
    free(r->hostname);
    free(r->buffer);
}

void remote_terminate_jobs(Remote *r) {
    if (CLEAN_INFO) { fprintf(stderr, "Очистка очереди сообщений для %s...\n", r->hostname); }
    for (int i = r->mq.head; i < r->mq.tail; i++)
        freejob(&r->mq.items[i], REJECTED, "Хост был уничтожен преждевременно\n");
}

const char *strstate(Remote_state state) {
    switch (state) {
        case RS_NONE:
            return "RS_NONE";
        case RS_COMMAND_WAIT:
            return "RS_COMMAND_WAIT";
        case RS_COMMAND_OK:
            return "RS_COMMAND_OK";
        case RS_RESPONSE_WAIT:
            return "RS_RESPONSE_WAIT";
        case RS_RESPONSE_OK:
            return "RS_RESPONSE_OK";
        default:
            return "Unknown state";
    }
}

const char *strsmtp(Smtp_command state) {
    switch (state) {
        case SMTP_NONE:
            return "SMTP_NONE";
        case SMTP_GRET:
            return "SMTP_GRET";
        case SMTP_HELO:
            return "SMTP_HELO";
        case SMTP_MAIL:
            return "SMTP_MAIL";
        case SMTP_RCPT:
            return "SMTP_RCPT";
        case SMTP_DATA:
            return "SMTP_DATA";
        case SMTP_TEXT:
            return "SMTP_TEXT";
        case SMTP_RSET:
            return "SMTP_RSET";
        case SMTP_QUIT:
            return "SMTP_QUIT";
        default:
            return "Unknown state";
    }
}
