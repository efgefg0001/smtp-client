#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <poll.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <linux/limits.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>

#include "log.h"
#include "tools.h"
#include "config.h"

#define _INCLUDE_IMPLEMENTATION
#include "client.h"
#undef _INCLUDE_IMPLEMENTATION

extern int mta_is_running;

int mta_init(MTA *mta) {
    mta_remotes_dict_init(&mta->remotes, INITIAL_STRUCTURE_SIZE, (int (*)(char *, char *)) strcmp);
    pollfd_stack_init(&mta->descriptors, INITIAL_STRUCTURE_SIZE);
    mta->last_active_time = 0;
    return 0;
};

/**
 * Создать для каждого хоста исполнителя с очередью почтовых работ.
 * Если исполнитель уже сущесвует, то просто добавить писем к нему в очередь.
 */
void place_jobs(MTA *mta, Mailjob *jobs, int jobc) {
    if (USEFUL_INFO) { fprintf(stderr, "Распределение по очередям почтовых работ: %d \n", jobc); }
    /* проходим по списку всех работ */
    for (int i = 0; i < jobc; i++) {
        char *to = jobs[i].to_hostname;
        if (USEFUL_INFO) { fprintf(stderr, "Поступило работа на пересылку для %s\n", to); }
        Remote r;
        /* если в словаре отсутсвует ключ, то создадим новый удаленный хост, иначе сохраним r существующий */
        if (mta_remotes_dict_get(&mta->remotes, to, &r)) {
            remote_create(&r, to, mta->descriptors.count);
            pollfd_t pfd;
            pfd.fd = -1;
            /* сохраняем файловый дискриптор для данного хоста (потребуется в poll) */
            pollfd_stack_add(&mta->descriptors, pfd);
        }
        /* добавить в очередь сообщений */
        mailjob_queue_add(&r.mq, jobs[i]);
        /* создать новую запись в словаре или перезаписать значение по ключу */
        mta_remotes_dict_set(&mta->remotes, to, r);
    }
}


/**
 * Выполняем очередной шаг, возможно, переходим в следующее состояние
 * */
int mta_act(MTA *mta) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    /* получение текущего времени */
    long long now = tv.tv_sec * 1000LL + tv.tv_usec / 1000;
    /* подсчет времени ожидания */
    long long waiting = (now - mta->last_active_time);
    /* если удаленный хост обрабатывается впервые */
    if (mta->last_active_time == 0) { waiting = 500; }

    if (waiting >= 500) {
        mta->last_active_time = now;
        for (int i = 0; i < mta->remotes.count; i++) {
            Remote *r = &mta->remotes.items[i].value;
            if (r->state == RS_NONE && r->mq.count > 0) {
                pollfd_t *pfd = &mta->descriptors.items[r->pollfd_number];
                remote_act(r, pfd);
            }
        }
    }
    int pollr = poll(mta->descriptors.items, mta->descriptors.count, POLL_TIMEOUT_MSECS);
    if (pollr > 0) {
        for (int i = 0; i < mta->descriptors.count; i++) {
            pollfd_t *pfd = &mta->descriptors.items[i];
            if (pfd->revents > 0) {
                Remote *r;
                for (int j = 0; j < mta->remotes.count; j++) {
                    if (mta->remotes.items[j].value.pollfd_number == i) {
                        r = &mta->remotes.items[j].value;
                    }
                }
                remote_act(r, pfd);
            }
        }
    }
    return 0;
};

/**
 * Открываем maildir и создаем новый почтовые работы, проходясь по всем файлам
 * */
int find_new_mails(MTA *mta) {
    int finded_mails = 0;
    DIR *d; /* дирректория */
    struct dirent *file; /* файл внутри дирректории */
    const char *p = MAILDIR; /* путь к maildir*/
    d = opendir(p);
    if (!d) {
        fprintf(stderr, "Ошибка при попытке открыть дирректорию с письмами\n");
        exit(EXIT_FAILURE);
    }
    /* пройдется по всем файлам внутри дирректории */
    while (((file = readdir(d)) != NULL)) {
        /* пропуск всех нерегулярных файлов */
        if (file->d_type != DT_REG)
            continue;
        char path[PATH_MAX];
        int path_len = snprintf(path, sizeof(path) - 1, "%s/%s", p, file->d_name);
        path[path_len] = '\0';
        if (USEFUL_INFO) { fprintf(stderr, "Просматривается файл: %s!\n", path); }
        /* пропускаем те файлы, которые заблокированы или игнорируются */
        if (file_has_extension(path, ".ignore") || file_has_extension(path, ".lock")) {
            continue;
        }
        /* если для данного файла есть игнорируемая или блокируемая копия - тоже пропускаем */
        if (file_exists(path, ".ignore") || file_exists(path, ".lock")) {
            continue;
        }

        Mailjob *jobs = NULL;
        int jobc;

        /* если письмо не удалось распарсить, то помечаем его как игнорируемое */
        if (mail_to_jobs(path, &jobs, &jobc)) {
            fprintf(stderr, "Сообщение %s имеет некорректный формат и будет игнорировано\n", path);
            int buf_len = strlen(path) + strlen(".ignore") + 1;
            char *buf = calloc(buf_len, sizeof(char));
            snprintf(buf, buf_len, "%s%s", path, ".ignore");
            FILE *f = fopen(buf, "a");
            fclose(f);
            free(buf);
            continue;
        }

        /* блокируем файл, создавая его копию с расширением */
        file_createlock(path, ".lock");
        /* раскидываем найденные почтовые работы очередям сообщений для разных хостов */
        place_jobs(mta, jobs, jobc);
        /* +1 распарсеное письмо */
        finded_mails++;
        free(jobs);
    }
    closedir(d);
    return finded_mails;
}

void mta_terminate_jobs(MTA *mta) {
    fprintf(stderr, "Отмена всех текущих работ на сервере...\n");

    for (int i = 0; i < mta->remotes.count; i++) {
        remote_terminate_jobs(&mta->remotes.items[i].value);
    }
}

int mta_destroy(MTA *mta) {
    fprintf(stderr, "Запущена очистка сервера...\n");

    for (int i = 0; i < mta->remotes.count; i++) {
        free(mta->remotes.items[i].key);
        remote_destroy(&mta->remotes.items[i].value);
    }

    mta_remotes_dict_free(&mta->remotes);

    while (mta->descriptors.count > 0) {
        pollfd_t pfd;
        pollfd_stack_pop(&mta->descriptors, &pfd);
        close(pfd.fd);
    }

    pollfd_stack_free(&mta->descriptors);
    return 0;
};

void start_mta(MTA *mta) {
    /* основной цикс */
    while (mta_is_running) {
        /* есть ли новые письма? */
        int num_of_new_mails = find_new_mails(mta);
        if (num_of_new_mails) {
            if (USEFUL_INFO) { fprintf(stderr, "Найдено новых писем: %d\n", num_of_new_mails); }
        }
        int next = 1;
        /* работаем, пока все исполнители не отправят все письма */
        while (next && mta_is_running) {
            next = 0;
            mta_act(mta);
            /* проверяем все ли отработали */
            for (int i = 0; i < mta->remotes.count; ++i) {
                Remote *r = &mta->remotes.items[i].value;
                if (r->state != RS_NONE || r->mq.count > 0) {
                    next = 1;
                    break;
                }
            }
        }
        /* программам тоже нужно отдыхать */
        if (USEFUL_INFO) { printf("Фух, можно передохнуть секунд пять...\n"); }
        sleep(5);
    }
}
