#include <signal.h>

#include "log.h"
#include "client.h"
#include "config.h"

MTA mta;

int mta_is_running = 1;

/* обработчик, который сработает при завершении программы */
static void exit_handler() {
    if (CLEAN_INFO) { fprintf(stderr, "Прекращение работы сервера...\n"); }
    mta_destroy(&mta);
    if (CLEAN_INFO) { fprintf(stderr, "Работа сервера успешно остановлена.\n"); }
}

/* обработчик прерывания (в основном от Ctrl + C) */
static void ctrl_c_handler(int sig_num) {
    mta_is_running = 0;
}

/* начало */
int main(int argc, char *argv[]) {
    signal(SIGINT, ctrl_c_handler); /* установка обработчика */
    log_init();
    atexit(exit_handler); /* обработчик завершения программы */
    mta_init(&mta);
    start_mta(&mta);
    return 0;
}
