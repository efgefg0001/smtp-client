#include <errno.h>

#include "tools.h"
#include "jobs.h"

#define _INCLUDE_IMPLEMENTATION

typedef char *mta;

#define _ITEM_TYPE mta
#define _TYPED_NAME(x) mta_##x

#include "stack.h"

#undef _TYPED_NAME
#undef _ITEM_TYPE

#define _KEY_TYPE mta
#define _VALUE_TYPE mta_stack
#define _TYPED_NAME(x) mta_stk_##x

#include "dict.h"

#undef _TYPED_NAME
#undef _KEY_TYPE
#undef _VALUE_TYPE

#undef _INCLUDE_IMPLEMENTATION

/**
 * @param[in] file_name Имя файла, который будет проанализирован.
 * @param[out] jobs Указатель на массив почтовых работ, в который будут сохранен список работ. Следует передавать NULL указатель.
 * @param[out] jobc Указатель на int, куда будет записано число обнаруженных почтовых работ.
 * @return Возвращает 0 в случае успеха и -1, если письмо не удалось распарсить.
 */
int mail_to_jobs(const char *file_name, Mailjob **jobs, int *jobc) {
    int r = -1; /* результат возврата ф-ции по-умолчанию */
    const int delimeters[] = {' ', '\n', '@'}; /* возможные разделители */
    int delimeter_found; /* найденный разделитель */

    /* словарь: ключ - имя хоста, значение - стек получателей */
    mta_stk_dict recipients;
    mta_stk_dict_init(&recipients, 20, (int (*)(char *, char *)) strcmp);

    FILE *f = fopen(file_name, "r");
    if (f == NULL) {
        fprintf(stderr, "Не могу отрыть файл: %s\n", strerror(errno));
        goto cleanup_0;
    }
    char *string = NULL;
    size_t str_len;
    // freed before exit, in cleanup_1
    /* библиотечкая ф-ция: считывает целую строку из потока аналогично getlin но с нестандартным разделителем */
    /* string и strl_len будут обновлены */
    /* есть ли mailto */
    getdelim(&string, &str_len, ' ', f);
    if (strcmp("mailto: ", string)) {
        fprintf(stderr, "Не найдено поле \"mailto:\"\n");
        goto cleanup_1;
    }
    do { /* парсим mailto: */
        /* пользователь */
        char *name = NULL;
        size_t name_size;
        get_delimiters(&name, &name_size, delimeters, 3, &delimeter_found, f);
        if (delimeter_found != '@') {
            fprintf(stderr, "Ошибка при разборе имени получателя в \"mailto\"\n");
            free(name);
            goto cleanup_2;
        }
        /* хост */
        char *host = NULL;
        size_t host_len;
        get_delimiters(&host, &host_len, delimeters, 3, &delimeter_found, f);
        /* имя хоста заканчивается переходом на новую строку или пробелом */
        if (delimeter_found != ' ' && delimeter_found != '\n') {
            fprintf(stderr, "Ошибка при разборе имени хоста\n");
            free(name);
            free(host);
            goto cleanup_2;
        }
        mta_stack sstk; /* новый пусто стек */
        int ret;
        /* если в словаре отсутствует указанный хост
         * ТО инициализирует новый стек
         * ИНАЧЕ сохранит указатель на сущесвующий в переменную sstk
         * */
        if ((ret = mta_stk_dict_get(&recipients, host, &sstk))) {
            mta_stack_init(&sstk, 20);
        }
        /* добавить получателя в стек к другим получателям */
        mta_stack_add(&sstk, name);
        /* добавит новый ключ и значение в словарь или перезапишет существующее значение */
        mta_stk_dict_set(&recipients, host, sstk);
        /* если хост с таким именем уже есть в словаре, то очистим переменную
         * иначе ссылка на неё будет использована в словаре и будет освобождена в момент удаления словаря */
        if (ret == 0) {
            free(host);
        }
        /* как только дошли до конца строки выходим из цикла */
        if (delimeter_found == '\n') {
            break;
        }
    } while (1);
    /* парсим mailfrom: */
    char *temp_buf = NULL;
    size_t temp_buf_len, temp_buf_l, ssrv_l;
    /* mailfrom */
    temp_buf_l = get_delimiters(&temp_buf, &temp_buf_len, delimeters, 3, &delimeter_found, f);
    if (delimeter_found != ' ') { /* ладно, тут может быть все что угодно кроме mailfrom :) */
        fprintf(stderr, "Не найдено поле \"mailfrom:\"\n");
        goto cleanup_3;
    }
    /* пользователь */
    temp_buf_l = get_delimiters(&temp_buf, &temp_buf_len, delimeters, 3, &delimeter_found, f);
    if (delimeter_found != '@') {
        fprintf(stderr, "Ошибка при разборе отправителя\n");
        goto cleanup_3;
    }
    /* хост */
    char *ssrv = NULL;
    size_t ssrv_len;
    ssrv_l = get_delimiters(&ssrv, &ssrv_len, delimeters, 3, &delimeter_found, f);
    if (delimeter_found != '\n') {
        fprintf(stderr, "Ошибка при разборе отправителя\n");
        goto cleanup_4;
    }
    /* содержимое письма, оно же body */
    char *content = NULL;
    size_t content_len, content_l;
    content_l = getdelim(&content, &content_len, EOF, f);
    if (content_l <= 0) {
        fprintf(stderr, "Письмо без содержания! Ошибка!\n");
        goto cleanup_5;
    }
    /* число найденных работе */
    (*jobc) = recipients.count;
    /* выделить память под массив */
    *jobs = malloc(sizeof(Mailjob) * recipients.count);

    /* заполняем структуру письма, которое будет одно на несколько получателей */
    Mail *m = malloc(sizeof(Mail));
    m->content = content;
    m->sender = calloc(temp_buf_l + ssrv_l + 1, sizeof(char));
    snprintf(m->sender, temp_buf_l + ssrv_l + 4, "%s@%s", temp_buf, ssrv);
    m->num_of_jobs = (*jobc);
    m->file_name = calloc((strlen(file_name) + 10), sizeof(char)); /* ? это подозрительное метосто ломает тесты! */
    strcpy(m->file_name, file_name);
    /* на каздого получателя заводим новую работу */
    for (int i = 0; i < recipients.count; i++) {
        Mailjob mj;
        mj.mail = m;
        mj.skipped = 0;
        mta_stk_tuple tup = recipients.items[i];
        mj.to_hostname = tup.key;
        mj.recipients = malloc(sizeof(char *) * tup.value.count);
        mj.recipients_count = tup.value.count;
        for (int j = 0; j < tup.value.count; j++)
            mj.recipients[j] = tup.value.items[j];
        (*jobs)[i] = mj;
        /* освободить стек в котором хранились получатели на указанном хосте */
        mta_stack_free(&tup.value);
    }
    r = 0;
    free(temp_buf);
    free(ssrv);
    goto cleanup_1;
    cleanup_5: /* need to free content buffer */
    free(content);
    cleanup_4: /* need to free sender srv */
    free(ssrv);
    cleanup_3: /* need to free sender name */
    free(temp_buf);
    cleanup_2: /* need to free insides of recipients */
    for (int i = 0; i < recipients.count; i++) {
        mta_stk_tuple tup = recipients.items[i];
        for (int j = 0; j < tup.value.count; j++)
            free((void *) tup.value.items[j]);
        mta_stack_free(&tup.value);
        free(tup.key);
    }
    cleanup_1: /* early error or success */
    fclose(f);
    free(string);
    cleanup_0:
    mta_stk_dict_free(&recipients);
    return r;
}

/**
 * @brief Помечает письмо как игнорируемое, указывая пользователя, которого отверг удаленный сервер.
 * @param job Указатель на Mailjob.
 * @param skipped_user Номер в текущей работе по порядку пользователя, который был отвергнут удаленным сервером.
 */
void logjobskip(Mailjob *job, int skipped_user) {
    char buf[MAX_BUFFER];
    snprintf(buf, MAX_BUFFER, "%s%s", job->mail->file_name, ".ignore");
    FILE *f = fopen(buf, "a");
    fprintf(f, "User <%s@%s> was rejected by its server\n", job->recipients[skipped_user], job->to_hostname);
    fclose(f);
}

/**
 * @brief Помечает письмо, как игнорируемое, указывая причину.
 * @param job Указатель на Mailjob.
 * @param reason Причина.
 * */
void logmail_rejected(Mailjob *job, const char *reason) {
    char buf[MAX_BUFFER];
    snprintf(buf, MAX_BUFFER, "%s%s", job->mail->file_name, ".ignore");
    FILE *f = fopen(buf, "a");
    fprintf(f, "Mail for users ");
    for (int i = 0; i < job->recipients_count; i++) {
        fprintf(f, "<%s@%s> ", job->recipients[i], job->to_hostname);
    }
    fprintf(f, " was REJECTED with message %s\n", reason);
    fclose(f);
}

/**
 * @brief Освобождение ресурсов, выделенный для Mailjob, обрабатывая причину особождения.
 * @param job Указатель на Mailjob.
 * @param free_status Статус завершения: REJECTED или DELIVERED.
 * @param reason Причина по которой письмо было отвергнуто.
 */
void freejob(Mailjob *job, MJ_status free_status, const char *reason) {
    /* если письмо было отвергнуто удаленным сервером */
    if (free_status == REJECTED) {
        logmail_rejected(job, reason);
    }

    /* очистить память, выделенную для каждого из получателей */
    for (int i = 0; i < job->recipients_count; i++)
        free(job->recipients[i]);

    free(job->recipients); /* освободить массив указателей на отправителей */

    --(job->mail->num_of_jobs); /* уменьшить счетчик ссылок на письмо */
    if (job->mail->num_of_jobs == 0) { /* счетчик ссылок достиг нуля -> файл письма может быть удален */
        file_removelock(job->mail->file_name, ".lock");
        if (!file_exists(job->mail->file_name, ".ignore")) { /* если сущ. файл письма, который игнорируется */
            remove(job->mail->file_name); /* удаляем и его */
        }
        /* освободить память выделенную под отправителя, содержимое письма и название файла */
        free(job->mail->sender);
        free(job->mail->content);
        free(job->mail->file_name);
    }
}

/**
 * @brief Выводид содержимое массива элементов Mailjob, который был получен в результате анализа одного письма.
 * @param jobs Указатель на массив.
 * @param jobc Число элементов в массиве.
 * */
void printjobs(Mailjob *jobs, int *jobc) {
    printf("Mail from: %s\n", jobs[0].mail->sender);
    printf("Hostname: %s\n", jobs[0].to_hostname);
    printf("Mail content:\n-----\n%s\n-----\n", jobs[0].mail->content);
    printf("Внутри функции печати!\n");
    for (int i = 0; i < *jobc; i++) {
        printf("\nJob %d:\n", i);
        printf("\tserver: %s\n", jobs[i].to_hostname);
        printf("\tusers: %d\n", jobs[i].recipients_count);
        for (int j = 0; j < jobs[i].recipients_count; j++) {
            printf("\t\t%s\n", jobs[i].recipients[j]);
        }
    }
}
