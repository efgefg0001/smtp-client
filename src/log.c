#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <mqueue.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>

#include "config.h"
#define _INCLUDE_IMPLEMENTATION
#include "log.h"
#undef _INCLUDE_IMPLEMENTATION

static int proc_creator = 0;
static pid_t log_pid = 0;
static int inited = 0;

static mqd_t log_fd; /* дискриптор очереди сообщений */

static int logger_work = 1; /* флаг для остановки работы логера */

static void log_killer() {
    if (proc_creator == getpid()) {
        if (CLEAN_INFO) { fprintf(stderr, "Логгер прекращает работу...\n"); }
        kill(log_pid, SIGTERM);
    }
}

/* обработчик - прекращает работу логгера */
static void sig_handler(int sig_num) {
    if (CLEAN_INFO) { fprintf(stderr, "Остановка работы логгера...\n"); }
    logger_work = 0;
}

static void log_run() {
    /* установка обработчика на сигнал об уничтожении процесса */
    struct sigaction act;
    memset (&act, '\0', sizeof(act));
    act.sa_handler = &sig_handler;
    sigemptyset(&act.sa_mask);
    sigaction(SIGTERM, &act, NULL);

    /* описать атрибутов для создания очереди сообщений*/
    struct mq_attr attr;
    attr.mq_flags   = 0;
    /* !!! mq_maxmsg по-умолчанию может быть только меньше 10
    * см. http://stackoverflow.com/questions/13929511/posix-message-queues-error-on-open-invalid-argument
     * */
    attr.mq_maxmsg  = 10;
    attr.mq_msgsize = MAX_MSG_SIZE; /* максимальный размер сообщения */
    attr.mq_curmsgs = 0;

    int flags = O_CREAT | O_RDONLY;
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;

    /* создать новую очередь сообщений на заданных параметров */
    log_fd = mq_open(QUEUE_NAME, flags, mode, &attr);

    if (log_fd == -1) {
        /* если во время предыдущего сеанса очередь сообщений не была отсоединена */
        mq_unlink(QUEUE_NAME);
        log_fd = mq_open(QUEUE_NAME, flags, mode, &attr);
        if (log_fd == -1) {
            fprintf(stderr, "ошибка при создании очереди сообщений в ф-ции log_init()\n");
            exit(EXIT_FAILURE);
        }
    }

    FILE *logfile = fopen(LOG_PATH, "a");
    if (logfile == NULL) {
        fprintf(stderr, "ошибка при попытке открытия/создания лог-файла\n");
        exit(EXIT_FAILURE);
    }

    time_t now;
    char date_time[DT_LENGTH]; /* строковое представление времени */

    int size;
    char log_str[MAX_MSG_SIZE]; /* сообщение */
    while (logger_work) {
        /* до тех пор, пока не поступит сигнал о прекращении работы логгера - прослушивать очередь */
        size = mq_receive(log_fd, log_str, MAX_MSG_SIZE, NULL);
        if (size > 0) {
            now = time(NULL);
            strftime(date_time, DT_LENGTH, "%Y-%m-%d %H:%M:%S", localtime(&now));
            /* запись текущей даты, времени и сообщения в лог-файл */
            fprintf(logfile, "%s %s\n", date_time, log_str);
            fflush(logfile);
        }
    }
    /* освободить дискрипторы */
    mq_close(log_fd);
    mq_unlink(QUEUE_NAME);
    fclose(logfile);
    sync();
}

void write_to_log(const char *msg) {
    if (inited != getpid()) {
        mq_close(log_fd);
        log_fd = mq_open(QUEUE_NAME, O_WRONLY);
        if (log_fd == -1) {
            fprintf(stderr, "ошибка при открытии очереди сообщений в ф-ции write_to_log()\n");
            exit(EXIT_FAILURE);
        }
        inited = getpid();
    }
    log_fd = mq_open(QUEUE_NAME, O_WRONLY);
    if (log_fd == -1) {
        fprintf(stderr, "ошибка при отправке сообщения в ф-ции write_to_log()\n");
        exit(EXIT_FAILURE);
    }
    char log_str[MAX_MSG_SIZE];
    snprintf(log_str, sizeof(log_str), "%s", msg);
    int res = mq_send(log_fd, log_str, MAX_MSG_SIZE, 1);
    if (res < 0) {
        fprintf(stderr, "ошибка при отправке сообщения в ф-ции write_to_log()\n");
    }
}

/**
 * @brief Запись в лог. Поддерживает формат в стиле printf.
 * */
void to_log(const char *format, ...) {
    va_list vl;
    va_start(vl, format);
    int buf_len = MAX_MSG_SIZE;
    char buf[buf_len];
    vsnprintf(buf, buf_len, format, vl);
    write_to_log((const char *) buf);
    va_end(vl);
}

/**
 * @brief Инициализация лога. Нужно запустить перед тем как писать в него.
 * При этом нужно дать возможность дочернему процессу инициализировать соответствующие структуры.
 * */
void log_init() {
    proc_creator = getpid();
    pid_t pid = fork();
    if (pid == 0) {
        log_run();
        if (CLEAN_INFO) { fprintf(stderr, "Логгер успешно завершил работу.\n"); }
        exit(EXIT_SUCCESS);
    } else {
        /* дать возможность дочернему процессу инициализировать очередь сообщений */
        sleep(1);
    }
    log_pid = pid;
    atexit(log_killer);
    return;
}