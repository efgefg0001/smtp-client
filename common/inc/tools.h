#ifndef FUTILS_H
#define FUTILS_H

#include <stdio.h>

/**
 * @brief Считывает файловый до тех пор пока не встретится один из указанных разделителей, сохраняя считанные символы в буфер.
 * @param[out] buf Указатель на буфер, куда будет сохранен результат. Если буфер NULL - для него будет выделена память.
 * @param[out] buf_size Указатель на переменную куда будет сохранен размер буфера.
 * @param[in] delimiters Массив символов, которые будут интерпретированы как разделители.
 * @param[in] num_of_delimiters Число элементов в массиве разделителей.
 * @param[out] delimeter_found Вернет первый встретившийся разделитель или -1, если достигнут конец файла.
 * @param stream Указатель на файл.
 * @return Числи символов, которое было считано до разделителя.
 */
size_t get_delimiters(char **buf, size_t *buf_size, const int *delimiters, int num_of_delimiters, int *delimeter_found, FILE *stream);

/**
 * @brief Проверяет, с
 * @param file_name Путь к файлу.
 * @param extension Расширение файла вместе с точкой. Например, '.txt'
 * @return Единицу, если файл существует, иначе ноль.
 */
int file_exists(const char *file_name, const char *extension);

/**
 * @brief Проверяет, имеет ли файл указанное расширение.
 * @param file_name Путь к файлу.
 * @param extension Расширение файла вместе с точкой. Например, '.txt'
 * @return Единицу, если файл имеет указанное расширение, иначе ноль.
 */
int file_has_extension(const char *file_name, const char *extension);

/**
 * @brief Создает заблокированную копию указанного файла.
 * @param file_name Путь к файлу.
 * @param extension Расширение с точкой, которое будет добавлено к файлу. Например, '.lock'
 */
void file_createlock(const char *file_name, const char *extension);

/**
 * @brief Удаляет заблокированный файл с указанным расширением.
 * @param file_name Путь к файлу.
 * @param extension Расширение файла вместе с точкой. Например, '.txt'
 * @return Единицу, если файл имеет указанное расширение, иначе 0.
 */
void file_removelock(const char *file_name, const char *extension);

#endif
