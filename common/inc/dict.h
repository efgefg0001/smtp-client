#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* типы данных по-умолчанию */
#if !defined(_KEY_TYPE) || !defined(_VALUE_TYPE) || !defined(_TYPED_NAME)
#define _KEY_TYPE int
typedef char *charptr;
#define _VALUE_TYPE charptr
#define _TYPED_NAME(x) int_charptr_##x
#endif

#define DICT_GROWTH_STEP 10

/* структурка для словаря: ключ и значение, ничего лишнего */
typedef struct {
    _KEY_TYPE key;
    _VALUE_TYPE value;
} _TYPED_NAME(tuple);

typedef struct {
    _TYPED_NAME(tuple) *items; /* список элементов */
    int count; /* текущее число элементов */
    int maxsize; /* текущий максимальный размер */
    int (*compare)(_KEY_TYPE, _KEY_TYPE); /* указатель на функцию для сравнения ключей */
} _TYPED_NAME(dict);

/**
 * @brief Инициализация списка.
 * @param dict Указатель на список.
 * @param maxsize Текущий максимальный размер.
 * @param compare Функция, которая будут использоваться для сравнения ключей.
 */
void _TYPED_NAME(dict_init)(_TYPED_NAME(dict) *dict, int maxsize, int (*compare)(_KEY_TYPE, _KEY_TYPE))
#ifdef _INCLUDE_IMPLEMENTATION
{
    dict->items = malloc(sizeof(_TYPED_NAME(tuple)) * maxsize);
    dict->count = 0;
    dict->maxsize = maxsize;
    dict->compare = compare;
}
#else
;

#endif

/**
 * @brief Очистка списка.
 * @param dict Указатель на список. Обо всем остальном мы позаботимся сами :)
 */
void _TYPED_NAME(dict_free)(_TYPED_NAME(dict) *dict)
#ifdef _INCLUDE_IMPLEMENTATION
{
    free(dict->items);
    dict->count = 0;
    dict->maxsize = 0;
}
#else
;

#endif

/**
 * @brief Перезапишет значение по ключу, если совпадение найдено, иначе создаст новую пару ключ + значение
 * @param dict Указатель на список.
 * @param[in] key Ключ.
 * @param[in] value Значение.
 * @return Так или иначе вернет ноль.
 */
int _TYPED_NAME(dict_set)(_TYPED_NAME(dict) *dict, _KEY_TYPE key, _VALUE_TYPE value)
#ifdef _INCLUDE_IMPLEMENTATION
{
    for(int i = 0; i < dict->count; i++) {
        if (!dict->compare(dict->items[i].key, key)) {
            dict->items[i].value = value;
            return 0;
        }
    }
    /* перевыделение памяти */
    if (dict->count == dict->maxsize) {
        _TYPED_NAME(tuple) * newitems =
            malloc(sizeof(_TYPED_NAME(tuple)) * (dict->maxsize + DICT_GROWTH_STEP));
        memcpy(newitems, dict->items, dict->maxsize * sizeof(_TYPED_NAME(tuple)));
        free(dict->items);
        dict->items = newitems;
        dict->maxsize += DICT_GROWTH_STEP;
    } {
        /* dict->count < dict->maxsize */
        _TYPED_NAME(tuple) tup;
        tup.key = key;
        tup.value = value;
        dict->items[dict->count++] = tup;
        return 0;
    }
}
#else
;

#endif

/**
 * @brief Получить значение по ключу и в случае успех сохранить его в переменной value.
 * @param dict Указатель на список.
 * @param[in] key Ключ.
 * @param[out] value Значение.
 * @return Ноль если данный ключ найден, иначе -1;
 */
int _TYPED_NAME(dict_get)(_TYPED_NAME(dict) *dict, _KEY_TYPE key, _VALUE_TYPE *value)
#ifdef _INCLUDE_IMPLEMENTATION
{
    for(int i = 0; i < dict->count; i++) {
        if (!dict->compare(dict->items[i].key, key)) {
            *value = dict->items[i].value;
            return 0;
        }
    }

    return -1;
}
#else
;

#endif
