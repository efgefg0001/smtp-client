#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* тип данных по умолчанию */
#if !defined(_ITEM_TYPE) || !defined(_TYPED_NAME)
#define _ITEM_TYPE int
#define _TYPED_NAME(x) int_##x
#endif

#define STACK_GROWTH_STEP 10

typedef struct {
    _ITEM_TYPE *items; /* элементы */
    int count; /* их количество */
    int maxsize; /* текущий максимальный размер */
} _TYPED_NAME(stack);

/**
 * @brief Инициализация стека.
 * @param stack Указатель на стек.
 * @param maxsize Текущий максимальный размер.
 */
void _TYPED_NAME(stack_init)(_TYPED_NAME(stack) *stack, int maxsize)
#ifdef _INCLUDE_IMPLEMENTATION
{
    stack->items = malloc(sizeof(_ITEM_TYPE) * maxsize);
    stack->count = 0;
    stack->maxsize = maxsize;
}
#else
;
#endif

/**
 * @brief Очистит память, которую занимают элементы стека.
 * @param stack Указатель на стек.
 */
void _TYPED_NAME(stack_free)(_TYPED_NAME(stack) *stack)
#ifdef _INCLUDE_IMPLEMENTATION
{
    free(stack->items);
    stack->count = stack->maxsize = 0;
}
#else
;
#endif

/**
 * @brief Добавляет элемент в стек. Если стек заполнен, перевыделяет память для него.
 * @param stack Указатель на стек.
 * @param item Элемент который будет добавлен в стек.
 * @return Ноль в случае успешного добавления.
 */
int _TYPED_NAME(stack_add)(_TYPED_NAME(stack) *stack, _ITEM_TYPE item)
#ifdef _INCLUDE_IMPLEMENTATION
{
    /* перевыделение памяти для стека*/
    if (stack->count == stack->maxsize) {
        _ITEM_TYPE * newitems = malloc(sizeof(_ITEM_TYPE) * (stack->maxsize + STACK_GROWTH_STEP));
        memcpy(newitems, stack->items, stack->maxsize * sizeof(_ITEM_TYPE));
        free(stack->items);
        stack->items = newitems;
        stack->maxsize += STACK_GROWTH_STEP;
    } {
        stack->items[stack->count++] = item;
        return 0;
    }
}
#else
;
#endif

/**
 * @brief Извлечь элемент из стека и сохранить его в item.
 * @param stack Указатель на стек.
 * @param[out] item Указатель на переменную куда сохранится извлеченный элемент.
 * @return Ноль в случае успеха, и -1, если стек пуст.
 */
int _TYPED_NAME(stack_pop)(_TYPED_NAME(stack) *stack, _ITEM_TYPE *item)
#ifdef _INCLUDE_IMPLEMENTATION
{
    if (stack->count > 0) {
        *item = stack->items[--stack->count];
        return 0;
    } else {
        return -1;
    }
}
#else
;
#endif