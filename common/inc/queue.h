#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if !defined(_ITEM_TYPE) || !defined(_TYPED_NAME)
#define _ITEM_TYPE int
#define _TYPED_NAME(x) int_##x
#endif

typedef struct
{
    _ITEM_TYPE * items;
    int count;
    int maxsize;
    int head;
    int tail;
} _TYPED_NAME(queue);

void _TYPED_NAME(queue_init)(_TYPED_NAME(queue) * queue, int maxsize)
#ifdef _INCLUDE_IMPLEMENTATION
{
    queue->items = malloc(sizeof(_ITEM_TYPE) * maxsize);
    queue->count = 0;
    queue->head = 0;
    queue->tail = 0;
    queue->maxsize = maxsize;
}
#else
;
#endif

void _TYPED_NAME(queue_free)(_TYPED_NAME(queue) * queue)
#ifdef _INCLUDE_IMPLEMENTATION
{
    free(queue->items);
    queue->count = 0;
    queue->head = 0;
    queue->tail = 0;
    queue->maxsize = 0;
}
#else
;
#endif

int _TYPED_NAME(queue_add)(_TYPED_NAME(queue) * queue, _ITEM_TYPE item)
#ifdef _INCLUDE_IMPLEMENTATION
{
    if (queue->head > 0 && queue->tail == queue->maxsize) {
        memmove(queue->items, queue->items + queue->head, sizeof(_ITEM_TYPE) * queue->count);
        queue->tail -= queue->head;
        queue->head = 0;
    }

    if (queue->count < queue->maxsize) {
        queue->items[queue->tail++] = item;
        queue->count++;
        return 0;
    } else {
        return -1;
    }
}
#else
;
#endif

int _TYPED_NAME(queue_pop)(_TYPED_NAME(queue) * queue, _ITEM_TYPE * value)
#ifdef _INCLUDE_IMPLEMENTATION
{
    if (queue->count > 0) {
        _ITEM_TYPE r = queue->items[queue->head++];
        queue->count--;
        *value = r;
        return 0;
    } else {
        return -1;
    }
}
#else
;
#endif