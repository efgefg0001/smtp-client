#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <sys/stat.h>

#include "tools.h"

#define BASE_BUF_SIZE 200
#define INC_STEP 50

/**
 * @brief Считывает файловый до тех пор пока не встретится один из указанных разделителей, сохраняя считанные символы в буфер.
 * @param[out] buf Указатель на буфер, куда будет сохранен результат. Если буфер NULL - для него будет выделена память.
 * @param[out] buf_size Указатель на переменную куда будет сохранен размер буфера.
 * @param[in] delimiters Массив символов, которые будут интерпретированы как разделители.
 * @param[in] num_of_delimiters Число элементов в массиве разделителей.
 * @param[out] delimeter_found Вернет первый встретившийся разделитель или -1, если достигнут конец файла.
 * @param stream Указатель на файл.
 * @return Числи символов, которое было считано до разделителя.
 */
size_t get_delimiters(char **buf, size_t *buf_size, const int *delimiters, int num_of_delimiters, int *delimeter_found, FILE *stream) {
    /* получаем флаги из потока */
    int flags = fcntl(fileno(stream), F_GETFL, 0);
    /* устанавлияем их + флаг для неблокировки */
    fcntl(fileno(stream), F_SETFL, flags | O_NONBLOCK);
    /* выделение памяти для буфера, если он пустой */
    if (*buf == NULL) {
        *buf_size = BASE_BUF_SIZE;
        *buf = calloc(BASE_BUF_SIZE, sizeof(char));
    }
    size_t str_len = 0;
    while (1) {
        /* перевыделение памяти для буфера */
        if (str_len + 1 >= *buf_size) {
            size_t new_size = *buf_size + INC_STEP;
            char *new_buf = malloc(sizeof(char) * new_size);
            memcpy(new_buf, *buf, str_len * sizeof(char));
            free(*buf);
            *buf = new_buf;
            *buf_size = new_size;
        }
        /* очередной символ из потока*/
        char c = fgetc(stream);
        /* если достигли конца файла */
        if (c == EOF) {
            /* устанавливаем метку разделителя */
            *delimeter_found = -1;
            goto ret;
        }
        for (int i = 0; i < num_of_delimiters; i++) {
            if (delimiters[i] == c) {
                *delimeter_found = c;
                goto ret;
            }
        }
        (*buf)[str_len++] = c;
    }
    ret:
        (*buf)[str_len] = '\0';
        fcntl(fileno(stream), F_SETFL, flags);
        return str_len;
}


/**
 * @brief Проверяет, с
 * @param file_name Путь к файлу.
 * @param extension Расширение файла вместе с точкой. Например, '.txt'
 * @return Единицу, если файл существует, иначе ноль.
 */
int file_exists(const char *file_name, const char *extension) {
    char path[PATH_MAX];
    int path_len = snprintf(path, sizeof(path) - 1, "%s%s", file_name, extension);
    path[path_len] = '\0';
    struct stat buffer;
    /* вносит в структуру buffer информацию из файла, который связан с path */
    int r = stat(path, &buffer);
    return (r == 0);
}

/**
 * @brief Проверяет, имеет ли файл указанное расширение.
 * @param file_name Путь к файлу.
 * @param extension Расширение файла вместе с точкой. Например, '.txt'
 * @return Единицу, если файл имеет указанное расширение, иначе ноль.
 */
int file_has_extension(const char *file_name, const char *extension) {
    /* поиск последнего вхождения в строку */
    char *dot = strrchr(file_name, '.');
    return dot && !strcmp(dot, extension);
}

/**
 * @brief Создает заблокированную копию указанного файла.
 * @param file_name Путь к файлу.
 * @param extension Расширение с точкой, которое будет добавлено к файлу. Например, '.lock'
 */
void file_createlock(const char *file_name, const char *extension) {
    char path[PATH_MAX];
    int path_len = snprintf(path, sizeof(path) - 1, "%s%s", file_name, extension);
    path[path_len] = '\0';
    int fd2 = open(path, O_RDWR | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);
    close(fd2);
}

/**
 * @brief Удаляет заблокированный файл с указанным расширением.
 * @param file_name Путь к файлу.
 * @param extension Расширение файла вместе с точкой. Например, '.txt'
 * @return Единицу, если файл имеет указанное расширение, иначе 0.
 */
void file_removelock(const char *file_name, const char *extension) {
    char path[PATH_MAX];
    int path_len = snprintf(path, sizeof(path) - 1, "%s%s", file_name, extension);
    path[path_len] = '\0';
    remove(path);
}
