CC := gcc
CFLAGS := -Wall -Werror -std=gnu99
LIB := -lrt -pthread
INC := -Icommon/inc -Iinc

# путь к почтовой дирректории относительно Makefile
MAILDIR := /home/alex/maildir
TESTS := tests

SRCDIR := src
COM_SRCDIR := common/src
BUILDDIR := build
TARGET := main

SRCEXT := c
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
SOURCES := $(SOURCES) $(shell find $(COM_SRCDIR) -type f -name *.$(SRCEXT))

all: $(TARGET)

$(TARGET): clean
	@echo "Building..."
	@mkdir $(BUILDDIR)
	$(CC) $(SOURCES) $(CFLAGS) $(INC) -o $(BUILDDIR)/$(TARGET) $(LIB)

run: poll

poll:
	@echo "Running..."
	@rm -rf $(MAILDIR)
	@mkdir $(MAILDIR)
	@cp $(TESTS)/mails/hosts-test/first/mail1 $(MAILDIR)/
	./$(BUILDDIR)/$(TARGET)

prepare_hosts:
	@echo "Внимание перезаписываю host.conf hosts!"
	@sudo cp -i $(TESTS)/hosts_files/* /etc/

prepare_mqueue:
	@sudo -i
	@echo 300 > /proc/sys/fs/mqueue/msg_max
	@exit

test_maildir:
	@if [ ! -d $(MAILDIR) ] ; \
	then \
		echo "Требуется создать почтовую дирректорию и убедиться, что путь укзан корректно в Makefile и config.h"; \
		exit 42; \
	fi; \

test_exim4:
	@echo "Running..."
	@rm -rf $(MAILDIR)
	@mkdir $(MAILDIR)
	@cp $(TESTS)/mails/exim4-test/* $(MAILDIR)/
	./$(BUILDDIR)/$(TARGET)

# запускать из под bash
test_hosts:
	@echo "Running..."
	@rm -rf $(MAILDIR)
	@mkdir $(MAILDIR)
	@sleep 8 && echo "Новые письма!" && cp $(TESTS)/mails/hosts-test/second/mail* $(MAILDIR) &
	@cp $(TESTS)/mails/hosts-test/first/mail* $(MAILDIR)/
	./$(BUILDDIR)/$(TARGET)

test_hightload:
	@echo "Running..."
	@rm -rf $(MAILDIR)
	@mkdir $(MAILDIR)
	@cp $(TESTS)/mails/hightload-test/* $(MAILDIR)/
	./$(BUILDDIR)/$(TARGET)

test_memcheck:
	@echo "Run memcheck"
	@valgrind --tool=memcheck --leak-check=full --show-reachable=yes ./$(BUILDDIR)/$(TARGET)

clean:
	@echo "Cleaning..."
	rm -rf $(BUILDDIR)

